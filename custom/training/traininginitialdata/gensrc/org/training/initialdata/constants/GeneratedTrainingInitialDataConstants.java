/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 14/01/2018 22:55:24                         ---
 * ----------------------------------------------------------------
 */
package org.training.initialdata.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTrainingInitialDataConstants
{
	public static final String EXTENSIONNAME = "traininginitialdata";
	
	protected GeneratedTrainingInitialDataConstants()
	{
		// private constructor
	}
	
	
}
